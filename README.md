How to install the software:

1. Go to directory info2300-assignment1\AYFinalProject\bin\Debug\AYFinalProject.exe
2. Run the .exe and the software/game will load up.

--------------------------------------------------------

LICENSE:
GNU General Public License V3.0

I have decided to use the General Public license because this license states that anyone can
run, study, share and modify the software. I want people to learn from my code if they are interested and
if they can improve the code then I can learn from them also.
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using Color = System.Drawing.Color;

namespace AYFinalProject
{
    public partial class AimTrainerForm : Form
    {
        PictureBox[] boxArray;
        int targetCount = 10;

        Stopwatch stopwatch = new Stopwatch();
        long elapsed_time;

        private readonly Random random = new Random();

        SoundPlayer clickSound = new SoundPlayer(AYFinalProject.Properties.Resources.KSHMR_Percussion_High_Clank_02_A_);

        int L = 0;

        public AimTrainerForm()
        {

            InitializeComponent();

            LoadMainMenu();
        }

        public void LoadMainMenu()
        {
            grpBoxMenu.Visible = true;
            if (lblHard.ForeColor == Color.White)
            {
                lblNormal.ForeColor = Color.Red;
            }
        }

        public void LoadHelpMenu()
        {
            grpBoxMenu.Visible = false;
            grpBoxHelp.Visible = true;
            grpBoxHelp.Location = new Point(338, 129);
        }

        public void LoadAboutMenu()
        {
            grpBoxMenu.Visible = false;
            grpBoxAbout.Visible = true;
            grpBoxAbout.Location = new Point(338, 129);
        }

        public void LoadNormalMode()
        {
            lblTargetCount.Visible = true;
            lblTargetRemaining.Visible = true;
            lblTargetCount.Text = targetCount.ToString();

            boxArray = new PictureBox[10];

            for (int i = 0; i < 10; i++)
            {
                boxArray[i] = new PictureBox();
                boxArray[i].Height = 40;
                boxArray[i].Width = 40;
                boxArray[i].BorderStyle = BorderStyle.FixedSingle;
                boxArray[i].BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
                boxArray[i].Tag = "0";
                boxArray[i].Click += PictureBox_Update;
                boxArray[i].BackColor = Color.White;

                this.Controls.Add(boxArray[i]);
            }

            for (int i = 1; i < 10; i++)
            {
                boxArray[i].Visible = false;
            }

            boxArray[0].Location = new Point(RandomNumber(0, 1025), RandomNumber(50, 550));
            // boxArray[0].Location = new Point(0,50);
        }

        public void LoadHardMode()
        {

            LoadNormalMode();
            timerMs.Enabled = true;

        }
        public void LoadGameFinished()
        {
            grpBoxGameFinished.Visible = true;

            lblResultMs.Text = (elapsed_time / 10).ToString() + " MS";
            grpBoxGameFinished.Location = new Point(260, 175);
        }

        public void PictureBox_Update(object sender, EventArgs e)
        {
            PictureBox clickedPictureBox = sender as PictureBox;

            if (lblNormal.ForeColor == Color.Red)
            {
                timerMs.Enabled = false;

                if (L == 9)
                {
                    clickSound.Play();

                    boxArray[9].Visible = false;
                    stopwatch.Stop();
                    timerMsShow.Enabled = false;
                    elapsed_time = stopwatch.ElapsedMilliseconds;

                    txtBoxMsElapsed.Text = elapsed_time.ToString();

                    LoadGameFinished();
                }

                if (L < 9)
                {
                    clickSound.Play();

                    stopwatch.Start();
                    timerMsShow.Enabled = true;

                    boxArray[L].Visible = false;
                    L++;
                    boxArray[L].Visible = true;
                    boxArray[L].Location = new Point(RandomNumber(0, 1025), RandomNumber(50, 550));

                }

                targetCount--;
                lblTargetCount.Text = targetCount.ToString();
            }
            else
            {
                timerMs.Enabled = true;


                if (L == 9)
                {
                    clickSound.Play();

                    boxArray[9].Visible = false;
                    stopwatch.Stop();
                    timerMsShow.Enabled = false;
                    elapsed_time = stopwatch.ElapsedMilliseconds;
                    txtBoxMsElapsed.Text = elapsed_time.ToString();

                    LoadGameFinished();
                }

                if (L < 9)
                {
                    clickSound.Play();

                    stopwatch.Start();
                    timerMsShow.Enabled = true;

                    boxArray[L].Visible = false;
                    L++;
                    boxArray[L].Visible = true;
                    boxArray[L].Location = new Point(RandomNumber(0, 1025), RandomNumber(50, 550));
                }

                targetCount--;
                lblTargetCount.Text = targetCount.ToString();
            }
        }

        private void timerMs_Tick(object sender, EventArgs e)
        {
            boxArray[L].Location = new Point(RandomNumber(250, 750), RandomNumber(200, 400));
        }

        public int RandomNumber(int min, int max)
        {
            return random.Next(min, max);
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblAbout_Click(object sender, EventArgs e)
        {
            LoadAboutMenu();
        }

        private void lblBackToMenuFromAbout_Click(object sender, EventArgs e)
        {
            LoadMainMenu();
            grpBoxAbout.Visible = false;
        }

        private void lblHelp_Click(object sender, EventArgs e)
        {
            LoadHelpMenu();
        }

        private void lblBackToMenuFromHelp_Click(object sender, EventArgs e)
        {
            grpBoxHelp.Visible = false;
            LoadMainMenu();
        }

        private void lblPlay_Click(object sender, EventArgs e)
        {
            grpBoxMenu.Visible = false;
            txtBoxMsElapsed.Visible = true;
            lblMsElapsed.Visible = true;

            if (lblNormal.ForeColor == Color.Red)
            {
                LoadNormalMode();
            }
            else
            {
                LoadHardMode();
            }
        }

        private void lblBackToMenuFromGameFinished_Click(object sender, EventArgs e)
        {
            elapsed_time = 0;
            targetCount = 10;
            L = 0;
            stopwatch.Reset();
            lblTargetCount.Visible = false;
            lblTargetRemaining.Visible = false;
            grpBoxGameFinished.Visible = false;
            timerMs.Enabled = false;
            txtBoxMsElapsed.Text = "";
            txtBoxMsElapsed.Visible = false;
            lblMsElapsed.Visible = false;

            LoadMainMenu();
        }

        private void lblNormal_Click(object sender, EventArgs e)
        {
            lblNormal.ForeColor = Color.Red;
            lblHard.ForeColor = Color.White;
        }

        private void lblHard_Click(object sender, EventArgs e)
        {
            lblNormal.ForeColor = Color.White;
            lblHard.ForeColor = Color.Red;
        }

    }


}

﻿
namespace AYFinalProject
{
    partial class AimTrainerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AimTrainerForm));
            this.timerMs = new System.Windows.Forms.Timer(this.components);
            this.lblTargetRemaining = new System.Windows.Forms.Label();
            this.lblTargetCount = new System.Windows.Forms.Label();
            this.txtBoxMsElapsed = new System.Windows.Forms.TextBox();
            this.grpBoxMenu = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblHard = new System.Windows.Forms.Label();
            this.lblNormal = new System.Windows.Forms.Label();
            this.lblDifficulty = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblAbout = new System.Windows.Forms.Label();
            this.lblHelp = new System.Windows.Forms.Label();
            this.lblPlay = new System.Windows.Forms.Label();
            this.grpBoxAbout = new System.Windows.Forms.GroupBox();
            this.lblBackToMenuFromAbout = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAbout2 = new System.Windows.Forms.Label();
            this.grpBoxHelp = new System.Windows.Forms.GroupBox();
            this.lblBackToMenuFromHelp = new System.Windows.Forms.Label();
            this.rchTxtBoxHelp = new System.Windows.Forms.RichTextBox();
            this.lblHelp2 = new System.Windows.Forms.Label();
            this.lblHelpExplanation = new System.Windows.Forms.Label();
            this.grpBoxGameFinished = new System.Windows.Forms.GroupBox();
            this.lblBackToMenuFromGameFinished = new System.Windows.Forms.Label();
            this.lblResultMs = new System.Windows.Forms.Label();
            this.lblAverageTime = new System.Windows.Forms.Label();
            this.lblMsElapsed = new System.Windows.Forms.Label();
            this.timerMsShow = new System.Windows.Forms.Timer(this.components);
            this.grpBoxMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpBoxAbout.SuspendLayout();
            this.grpBoxHelp.SuspendLayout();
            this.grpBoxGameFinished.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerMs
            // 
            this.timerMs.Interval = 500;
            this.timerMs.Tick += new System.EventHandler(this.timerMs_Tick);
            // 
            // lblTargetRemaining
            // 
            this.lblTargetRemaining.AutoSize = true;
            this.lblTargetRemaining.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTargetRemaining.ForeColor = System.Drawing.Color.White;
            this.lblTargetRemaining.Location = new System.Drawing.Point(377, 9);
            this.lblTargetRemaining.Name = "lblTargetRemaining";
            this.lblTargetRemaining.Size = new System.Drawing.Size(194, 25);
            this.lblTargetRemaining.TabIndex = 0;
            this.lblTargetRemaining.Text = "Target Remaining :";
            this.lblTargetRemaining.Visible = false;
            // 
            // lblTargetCount
            // 
            this.lblTargetCount.AutoSize = true;
            this.lblTargetCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTargetCount.ForeColor = System.Drawing.Color.White;
            this.lblTargetCount.Location = new System.Drawing.Point(577, 9);
            this.lblTargetCount.Name = "lblTargetCount";
            this.lblTargetCount.Size = new System.Drawing.Size(24, 25);
            this.lblTargetCount.TabIndex = 1;
            this.lblTargetCount.Text = "2";
            this.lblTargetCount.Visible = false;
            // 
            // txtBoxMsElapsed
            // 
            this.txtBoxMsElapsed.Location = new System.Drawing.Point(886, 15);
            this.txtBoxMsElapsed.Name = "txtBoxMsElapsed";
            this.txtBoxMsElapsed.ReadOnly = true;
            this.txtBoxMsElapsed.Size = new System.Drawing.Size(155, 20);
            this.txtBoxMsElapsed.TabIndex = 2;
            this.txtBoxMsElapsed.Visible = false;
            // 
            // grpBoxMenu
            // 
            this.grpBoxMenu.Controls.Add(this.groupBox1);
            this.grpBoxMenu.Controls.Add(this.lblExit);
            this.grpBoxMenu.Controls.Add(this.label4);
            this.grpBoxMenu.Controls.Add(this.lblAbout);
            this.grpBoxMenu.Controls.Add(this.lblHelp);
            this.grpBoxMenu.Controls.Add(this.lblPlay);
            this.grpBoxMenu.Location = new System.Drawing.Point(208, 129);
            this.grpBoxMenu.Name = "grpBoxMenu";
            this.grpBoxMenu.Size = new System.Drawing.Size(679, 378);
            this.grpBoxMenu.TabIndex = 3;
            this.grpBoxMenu.TabStop = false;
            this.grpBoxMenu.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblHard);
            this.groupBox1.Controls.Add(this.lblNormal);
            this.groupBox1.Controls.Add(this.lblDifficulty);
            this.groupBox1.Location = new System.Drawing.Point(6, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 203);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // lblHard
            // 
            this.lblHard.AutoSize = true;
            this.lblHard.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHard.ForeColor = System.Drawing.Color.White;
            this.lblHard.Location = new System.Drawing.Point(6, 138);
            this.lblHard.Name = "lblHard";
            this.lblHard.Size = new System.Drawing.Size(107, 37);
            this.lblHard.TabIndex = 6;
            this.lblHard.Text = "HARD";
            this.lblHard.Click += new System.EventHandler(this.lblHard_Click);
            // 
            // lblNormal
            // 
            this.lblNormal.AutoSize = true;
            this.lblNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNormal.ForeColor = System.Drawing.Color.White;
            this.lblNormal.Location = new System.Drawing.Point(6, 101);
            this.lblNormal.Name = "lblNormal";
            this.lblNormal.Size = new System.Drawing.Size(154, 37);
            this.lblNormal.TabIndex = 5;
            this.lblNormal.Text = "NORMAL";
            this.lblNormal.Click += new System.EventHandler(this.lblNormal_Click);
            // 
            // lblDifficulty
            // 
            this.lblDifficulty.AutoSize = true;
            this.lblDifficulty.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDifficulty.ForeColor = System.Drawing.Color.White;
            this.lblDifficulty.Location = new System.Drawing.Point(6, 16);
            this.lblDifficulty.Name = "lblDifficulty";
            this.lblDifficulty.Size = new System.Drawing.Size(211, 37);
            this.lblDifficulty.TabIndex = 5;
            this.lblDifficulty.Text = "DIFFICULTY:";
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(303, 311);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(87, 37);
            this.lblExit.TabIndex = 4;
            this.lblExit.Text = "EXIT";
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(303, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 37);
            this.label4.TabIndex = 3;
            this.label4.Text = "MENU:";
            // 
            // lblAbout
            // 
            this.lblAbout.AutoSize = true;
            this.lblAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAbout.ForeColor = System.Drawing.Color.White;
            this.lblAbout.Location = new System.Drawing.Point(303, 261);
            this.lblAbout.Name = "lblAbout";
            this.lblAbout.Size = new System.Drawing.Size(128, 37);
            this.lblAbout.TabIndex = 2;
            this.lblAbout.Text = "ABOUT";
            this.lblAbout.Click += new System.EventHandler(this.lblAbout_Click);
            // 
            // lblHelp
            // 
            this.lblHelp.AutoSize = true;
            this.lblHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelp.ForeColor = System.Drawing.Color.White;
            this.lblHelp.Location = new System.Drawing.Point(303, 211);
            this.lblHelp.Name = "lblHelp";
            this.lblHelp.Size = new System.Drawing.Size(100, 37);
            this.lblHelp.TabIndex = 1;
            this.lblHelp.Text = "HELP";
            this.lblHelp.Click += new System.EventHandler(this.lblHelp_Click);
            // 
            // lblPlay
            // 
            this.lblPlay.AutoSize = true;
            this.lblPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlay.ForeColor = System.Drawing.Color.White;
            this.lblPlay.Location = new System.Drawing.Point(303, 162);
            this.lblPlay.Name = "lblPlay";
            this.lblPlay.Size = new System.Drawing.Size(100, 37);
            this.lblPlay.TabIndex = 0;
            this.lblPlay.Text = "PLAY";
            this.lblPlay.Click += new System.EventHandler(this.lblPlay_Click);
            // 
            // grpBoxAbout
            // 
            this.grpBoxAbout.Controls.Add(this.lblBackToMenuFromAbout);
            this.grpBoxAbout.Controls.Add(this.label1);
            this.grpBoxAbout.Controls.Add(this.lblAbout2);
            this.grpBoxAbout.Location = new System.Drawing.Point(1055, 129);
            this.grpBoxAbout.Name = "grpBoxAbout";
            this.grpBoxAbout.Size = new System.Drawing.Size(405, 378);
            this.grpBoxAbout.TabIndex = 4;
            this.grpBoxAbout.TabStop = false;
            this.grpBoxAbout.Visible = false;
            // 
            // lblBackToMenuFromAbout
            // 
            this.lblBackToMenuFromAbout.AutoSize = true;
            this.lblBackToMenuFromAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBackToMenuFromAbout.ForeColor = System.Drawing.Color.White;
            this.lblBackToMenuFromAbout.Location = new System.Drawing.Point(65, 261);
            this.lblBackToMenuFromAbout.Name = "lblBackToMenuFromAbout";
            this.lblBackToMenuFromAbout.Size = new System.Drawing.Size(261, 37);
            this.lblBackToMenuFromAbout.TabIndex = 5;
            this.lblBackToMenuFromAbout.Text = "BACK TO MENU";
            this.lblBackToMenuFromAbout.Click += new System.EventHandler(this.lblBackToMenuFromAbout_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(65, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(274, 37);
            this.label1.TabIndex = 6;
            this.label1.Text = "AHMAD YAFTALI";
            // 
            // lblAbout2
            // 
            this.lblAbout2.AutoSize = true;
            this.lblAbout2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAbout2.ForeColor = System.Drawing.Color.White;
            this.lblAbout2.Location = new System.Drawing.Point(136, 30);
            this.lblAbout2.Name = "lblAbout2";
            this.lblAbout2.Size = new System.Drawing.Size(137, 37);
            this.lblAbout2.TabIndex = 5;
            this.lblAbout2.Text = "ABOUT:";
            // 
            // grpBoxHelp
            // 
            this.grpBoxHelp.Controls.Add(this.lblBackToMenuFromHelp);
            this.grpBoxHelp.Controls.Add(this.rchTxtBoxHelp);
            this.grpBoxHelp.Controls.Add(this.lblHelp2);
            this.grpBoxHelp.Location = new System.Drawing.Point(976, 556);
            this.grpBoxHelp.Name = "grpBoxHelp";
            this.grpBoxHelp.Size = new System.Drawing.Size(405, 378);
            this.grpBoxHelp.TabIndex = 5;
            this.grpBoxHelp.TabStop = false;
            this.grpBoxHelp.Visible = false;
            // 
            // lblBackToMenuFromHelp
            // 
            this.lblBackToMenuFromHelp.AutoSize = true;
            this.lblBackToMenuFromHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBackToMenuFromHelp.ForeColor = System.Drawing.Color.White;
            this.lblBackToMenuFromHelp.Location = new System.Drawing.Point(76, 310);
            this.lblBackToMenuFromHelp.Name = "lblBackToMenuFromHelp";
            this.lblBackToMenuFromHelp.Size = new System.Drawing.Size(261, 37);
            this.lblBackToMenuFromHelp.TabIndex = 7;
            this.lblBackToMenuFromHelp.Text = "BACK TO MENU";
            this.lblBackToMenuFromHelp.Click += new System.EventHandler(this.lblBackToMenuFromHelp_Click);
            // 
            // rchTxtBoxHelp
            // 
            this.rchTxtBoxHelp.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.rchTxtBoxHelp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rchTxtBoxHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rchTxtBoxHelp.ForeColor = System.Drawing.Color.White;
            this.rchTxtBoxHelp.Location = new System.Drawing.Point(50, 96);
            this.rchTxtBoxHelp.Name = "rchTxtBoxHelp";
            this.rchTxtBoxHelp.Size = new System.Drawing.Size(311, 198);
            this.rchTxtBoxHelp.TabIndex = 6;
            this.rchTxtBoxHelp.Text = resources.GetString("rchTxtBoxHelp.Text");
            // 
            // lblHelp2
            // 
            this.lblHelp2.AutoSize = true;
            this.lblHelp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelp2.ForeColor = System.Drawing.Color.White;
            this.lblHelp2.Location = new System.Drawing.Point(144, 30);
            this.lblHelp2.Name = "lblHelp2";
            this.lblHelp2.Size = new System.Drawing.Size(109, 37);
            this.lblHelp2.TabIndex = 5;
            this.lblHelp2.Text = "HELP:";
            // 
            // lblHelpExplanation
            // 
            this.lblHelpExplanation.AutoSize = true;
            this.lblHelpExplanation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelpExplanation.ForeColor = System.Drawing.Color.White;
            this.lblHelpExplanation.Location = new System.Drawing.Point(-85, 487);
            this.lblHelpExplanation.Name = "lblHelpExplanation";
            this.lblHelpExplanation.Size = new System.Drawing.Size(0, 20);
            this.lblHelpExplanation.TabIndex = 6;
            // 
            // grpBoxGameFinished
            // 
            this.grpBoxGameFinished.Controls.Add(this.lblBackToMenuFromGameFinished);
            this.grpBoxGameFinished.Controls.Add(this.lblResultMs);
            this.grpBoxGameFinished.Controls.Add(this.lblAverageTime);
            this.grpBoxGameFinished.Location = new System.Drawing.Point(127, 570);
            this.grpBoxGameFinished.Name = "grpBoxGameFinished";
            this.grpBoxGameFinished.Size = new System.Drawing.Size(574, 289);
            this.grpBoxGameFinished.TabIndex = 7;
            this.grpBoxGameFinished.TabStop = false;
            this.grpBoxGameFinished.Visible = false;
            // 
            // lblBackToMenuFromGameFinished
            // 
            this.lblBackToMenuFromGameFinished.AutoSize = true;
            this.lblBackToMenuFromGameFinished.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBackToMenuFromGameFinished.ForeColor = System.Drawing.Color.White;
            this.lblBackToMenuFromGameFinished.Location = new System.Drawing.Point(159, 220);
            this.lblBackToMenuFromGameFinished.Name = "lblBackToMenuFromGameFinished";
            this.lblBackToMenuFromGameFinished.Size = new System.Drawing.Size(261, 37);
            this.lblBackToMenuFromGameFinished.TabIndex = 6;
            this.lblBackToMenuFromGameFinished.Text = "BACK TO MENU";
            this.lblBackToMenuFromGameFinished.Click += new System.EventHandler(this.lblBackToMenuFromGameFinished_Click);
            // 
            // lblResultMs
            // 
            this.lblResultMs.AutoSize = true;
            this.lblResultMs.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultMs.ForeColor = System.Drawing.Color.White;
            this.lblResultMs.Location = new System.Drawing.Point(222, 130);
            this.lblResultMs.Name = "lblResultMs";
            this.lblResultMs.Size = new System.Drawing.Size(33, 37);
            this.lblResultMs.TabIndex = 5;
            this.lblResultMs.Text = "1";
            // 
            // lblAverageTime
            // 
            this.lblAverageTime.AutoSize = true;
            this.lblAverageTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAverageTime.ForeColor = System.Drawing.Color.White;
            this.lblAverageTime.Location = new System.Drawing.Point(49, 51);
            this.lblAverageTime.Name = "lblAverageTime";
            this.lblAverageTime.Size = new System.Drawing.Size(477, 37);
            this.lblAverageTime.TabIndex = 5;
            this.lblAverageTime.Text = "AVERAGE TIME PER TARGET:";
            // 
            // lblMsElapsed
            // 
            this.lblMsElapsed.AutoSize = true;
            this.lblMsElapsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsElapsed.ForeColor = System.Drawing.Color.White;
            this.lblMsElapsed.Location = new System.Drawing.Point(781, 15);
            this.lblMsElapsed.Name = "lblMsElapsed";
            this.lblMsElapsed.Size = new System.Drawing.Size(99, 20);
            this.lblMsElapsed.TabIndex = 8;
            this.lblMsElapsed.Text = "MS Elapsed:";
            this.lblMsElapsed.Visible = false;
            // 
            // timerMsShow
            // 
            this.timerMsShow.Interval = 1;
            // 
            // AimTrainerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(1069, 600);
            this.Controls.Add(this.lblMsElapsed);
            this.Controls.Add(this.grpBoxGameFinished);
            this.Controls.Add(this.lblHelpExplanation);
            this.Controls.Add(this.grpBoxHelp);
            this.Controls.Add(this.grpBoxMenu);
            this.Controls.Add(this.txtBoxMsElapsed);
            this.Controls.Add(this.lblTargetCount);
            this.Controls.Add(this.lblTargetRemaining);
            this.Controls.Add(this.grpBoxAbout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AimTrainerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aim Trainer";
            this.grpBoxMenu.ResumeLayout(false);
            this.grpBoxMenu.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpBoxAbout.ResumeLayout(false);
            this.grpBoxAbout.PerformLayout();
            this.grpBoxHelp.ResumeLayout(false);
            this.grpBoxHelp.PerformLayout();
            this.grpBoxGameFinished.ResumeLayout(false);
            this.grpBoxGameFinished.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerMs;
        private System.Windows.Forms.Label lblTargetRemaining;
        private System.Windows.Forms.Label lblTargetCount;
        private System.Windows.Forms.TextBox txtBoxMsElapsed;
        private System.Windows.Forms.GroupBox grpBoxMenu;
        private System.Windows.Forms.Label lblAbout;
        private System.Windows.Forms.Label lblHelp;
        private System.Windows.Forms.Label lblPlay;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.GroupBox grpBoxAbout;
        private System.Windows.Forms.Label lblBackToMenuFromAbout;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAbout2;
        private System.Windows.Forms.GroupBox grpBoxHelp;
        private System.Windows.Forms.Label lblHelp2;
        private System.Windows.Forms.Label lblHelpExplanation;
        private System.Windows.Forms.RichTextBox rchTxtBoxHelp;
        private System.Windows.Forms.Label lblBackToMenuFromHelp;
        private System.Windows.Forms.GroupBox grpBoxGameFinished;
        private System.Windows.Forms.Label lblResultMs;
        private System.Windows.Forms.Label lblAverageTime;
        private System.Windows.Forms.Label lblBackToMenuFromGameFinished;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblHard;
        private System.Windows.Forms.Label lblNormal;
        private System.Windows.Forms.Label lblDifficulty;
        private System.Windows.Forms.Label lblMsElapsed;
        private System.Windows.Forms.Timer timerMsShow;
    }
}

